import React, { Component } from 'react'
// import { createContainer } from 'meteor/react-meteor-data'

import { GlobalBar } from '../home'
import { SOFTWARE_NAME } from '../../../configs'
import { SketchPadInput } from '../../SketchPadInput'

export const ChangeBase = (e) => {
  console.log("change da base to", e)
}

export var currentBase = "dec"

export const HelpAbout = () => (
  <div>
  <div className='container ql-home-page'>
    <br />
    <br />
    <br />
    <div className='text-center'>
      <h1>About {SOFTWARE_NAME}</h1>
    </div>
    <br />
    <br />
    <div className='container'>
        <p>{SOFTWARE_NAME} is based off of the open-source project <a href="https://github.com/qlicker/qlicker">Qlicker</a>. Qlicker has the <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPL-3.0 License</a>, which means <a href="https://gitlab.com/EngrCS/real-time-audience-survey">the {SOFTWARE_NAME} source is publicly available under the same license</a>.</p>
        <p>{SOFTWARE_NAME} is currently being developed by Malcolm Anderson under Izad Khormaee.</p>
    </div>
  </div>
  </div>
  )