import { HelpBox, ForUserType } from './help_index'
import React from 'react'

export const HelpInst_CreateInst = {
    "title": "Create an institution",
    "for": 2,
    "items": [
                (<p>From the homepage, click <b><i className="material-icons">add_circle_outline</i>New</b> to the right of <i>My Institutions</i>. Confirm that you would like to create an institution, and then type a name for the institution. Click <b>Submit</b> to create the institution.</p>),
                (<p>Once created, the institution should show up underneath <i>My Institutions</i>. Your user account will be set to an institutional administrator for the institution, which will give you permission to promote and demote other institutional administrators.</p>)
            ]
}


export const HelpInst_ManageFaculty = {
    "title": "Manage institution faculty",
    "for": 2,
    "items": [
                (<h3>Adding faculty members</h3>),
                (<p>On the institution's page, click <b>Add a faculty member</b> and type the email address of the account you would like to add to the institution as a faculty member.</p>),
                (<p>If the box <b>Make this professor an institutional administrator</b> is not checked, the faculty member will be listed as a professor for the institution. This will allow them to create courses for the institution, but not manage the institution's settings or promote/demote other faculty members.</p>),
                (<div><br /></div>),
                (<h3>Promoting and demoting faculty members</h3>),
                (<p>Click the disclosure arrow to the right of the faculty member's name to manage their status. If they are an institutional administrator, you can demote them to a professor by clicking <b>Demote to professor</b>. If they are a professor, you can promote them to an institutional administrator by clicking <b>Promote to institutional administrator</b> or remove them from the institution entirely by clicking <b>Remove from institution</b>.</p>)
            ]
}